﻿CREATE TABLE missingPlay(
       outlook varchar(10),
       temperature float(5,2),
       umidity varchar(10),
       wind varchar(10),
       play varchar(10)
);


insert into missingPlay values('sunny',30.3,'high','weak','no');
insert into missingPlay values('sunny',30.3,'high','strong','no');
insert into missingPlay values('overcast',30.0,'high','weak','yes');
insert into missingPlay values('rain',13.0,'high','weak','yes');
insert into missingPlay values('rain',0.0,'normal','weak','yes');
insert into missingPlay values('rain',0.0,'normal','strong','no');
insert into missingPlay values('overcast',0.1,'normal','strong','');
insert into missingPlay values('sunny',13.0,'high','weak','no');
insert into missingPlay values('sunny',0.1,'normal','weak','yes');
insert into missingPlay values('rain',12.0,'normal','weak','yes');
insert into missingPlay values('sunny',,'normal','strong','yes');
insert into missingPlay values('overcast',12.5,'high','strong','yes');
insert into missingPlay values('overcast',29.21,'normal','weak','yes');
insert into missingPlay values('rain',12.5,'high','strong','no');
