package client_test;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import javax.swing.*;
import javax.swing.border.Border;
import java.awt.SystemColor;
import java.awt.Color;
import java.awt.Font;

/**
 * This class manages the GUI, so the GUI that allows the final user to perform frequent and closed patterns discovery
 * @author manos
 *
 */
public class Discovery extends JFrame{
	
	private static final long serialVersionUID = -5324770743310679025L;
	private PrintWriter out;
	private BufferedReader in;
	private JRadioButton db;
	private JRadioButton file;
	private JTextField nameDataTxt;
	private JButton runConstructionDB;
	private JTextField minSupTxt;
	private JTextField epsTxt;
	private JTextArea patternsAreaTxt;
	private JTextArea msgAreaTxt;
	private Socket socket;
	/**
	 * This constructor initializes some of the major layout components (background, font, style...)
	 */
	public Discovery() {
		getContentPane().setFont(new Font("Arial Black", getContentPane().getFont().getStyle(), getContentPane().getFont().getSize()));
		getContentPane().setBackground(SystemColor.activeCaption);
		setSize(1000, 800);
		addWindowListener(new WindowAdapter() {
			@Override
            public void windowClosing(WindowEvent we) {
				System.exit(1);
            }
		});

		try {
			InetAddress addr=InetAddress.getByName("127.0.0.1");
			if(addr==null) {
				msgAreaTxt.setText(msgAreaTxt.getText()+"\nError while connecting to server_test!");
				throw new ServerException("Error while connecting to server_test!");
			}
			socket=new Socket(addr,8080);
			out=new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
			in=new BufferedReader(new InputStreamReader(socket.getInputStream()));
			
			Container cp=getContentPane();//get info about applet
			cp.setLayout(new FlowLayout());
			JPanel north=new JPanel();
			north.setForeground(SystemColor.info);
			north.setBackground(SystemColor.controlHighlight);
			Border cases=BorderFactory.createTitledBorder("General informations");
			north.setBorder(cases);
			north.setLayout(new GridLayout());
			
			JPanel cpDiscoveryMining=new JPanel();
			cpDiscoveryMining.setBackground(SystemColor.control);
			Border borderTree=BorderFactory.createTitledBorder("Selecting Data Source");
			cpDiscoveryMining.setBorder(borderTree);
			cpDiscoveryMining.setLayout(new FlowLayout());
			ButtonGroup group=new ButtonGroup();
			db=new JRadioButton("Discovery patterns from db");
			file=new JRadioButton("Reading patterns from file");
			group.add(db);
			group.add(file);
			db.setSelected(true);
			cpDiscoveryMining.add(db);
			cpDiscoveryMining.add(file);
			north.add(cpDiscoveryMining);
			
			JPanel p=new JPanel();
			p.setBackground(SystemColor.control);
			Border border=BorderFactory.createTitledBorder("Input parameters");
			p.setBorder(border);
			p.setLayout(new FlowLayout());
			JLabel table=new JLabel("Insert table name:");
			nameDataTxt=new JTextField();
			nameDataTxt.setText("");
			nameDataTxt.setColumns(7);
			JLabel ms=new JLabel("Insert minSup:");
			minSupTxt=new JTextField();
			minSupTxt.setText("");
			minSupTxt.setColumns(3);
			JLabel eps=new JLabel("Insert epsilon:");
			epsTxt=new JTextField();
			epsTxt.setText("");
			epsTxt.setColumns(3);
			p.add(table);
			p.add(nameDataTxt);
			p.add(ms);
			p.add(minSupTxt);
			p.add(eps);
			p.add(epsTxt);
			north.add(p);
			cp.add(north);
			
			JPanel center=new JPanel();
			center.setBackground(SystemColor.control);
			Border exec=BorderFactory.createTitledBorder("Execution details");
			center.setBorder(exec);
			center.setLayout(new FlowLayout());
			runConstructionDB=new JButton("RUN");
			runConstructionDB.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 13));
			runConstructionDB.setAlignmentY(TOP_ALIGNMENT);
			runConstructionDB.setAlignmentX(CENTER_ALIGNMENT);
			center.add(runConstructionDB);
			patternsAreaTxt=new JTextArea();
			patternsAreaTxt.setForeground(SystemColor.infoText);
			patternsAreaTxt.setEditable(false);
			patternsAreaTxt.setColumns(70);
			patternsAreaTxt.setRows(20);
			patternsAreaTxt.setVisible(true);
			JScrollPane scrollArea=new JScrollPane(patternsAreaTxt);
			center.add(scrollArea);
			cp.add(center);
			
			JPanel south=new JPanel();
			south.setBackground(SystemColor.controlHighlight);
			Border extra=BorderFactory.createTitledBorder("Exeptions details");
			south.setBorder(extra);
			south.setLayout(new FlowLayout());
			msgAreaTxt=new JTextArea();
			msgAreaTxt.setForeground(Color.RED);
			msgAreaTxt.setEditable(false);
			msgAreaTxt.setColumns(70);
			msgAreaTxt.setRows(5);
			msgAreaTxt.setVisible(true);
			JScrollPane scrollV=new JScrollPane(msgAreaTxt);
			south.add(scrollV);
			cp.add(south);
			
			setPreferredSize(new Dimension(1000, 600));
			setMinimumSize(new Dimension(1000, 600));
			setMaximumSize(new Dimension(1000, 600));
			setSize(1000,650);
			setName("Let's mine!");
			setVisible(true);
			
			runConstructionDB.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					try {
						startDiscovery(e);
					} catch (IOException e1) {
						e1.printStackTrace();
						msgAreaTxt.setText("No patterns discovered due to a server_test failure! See more in the bottom area.");
						msgAreaTxt.setText(e1.getMessage());
					}
				}
			});
		} catch(ServerException se) {
			msgAreaTxt.setText(msgAreaTxt.getText()+"\n"+se.getMessage());
		} catch(IOException re) {
			msgAreaTxt.setText(msgAreaTxt.getText()+"\n"+re.getMessage());
		}
	}
	
	/**
	 * This method is used to handle events when the "RUN" button is pressed:
	 * it starts the action that the user wants (is db is selected, it starts mining; otherwise, it reads from file)
	 * @param e the ActionEvent: the click of the mouse on the "RUN" button
	 * @throws IOException but not handles the case in which, for example, the file doesn't exists.
	 */
	public void startDiscovery(ActionEvent e) throws IOException {
		patternsAreaTxt.setText(null);
		if(db.isSelected()) {//case 2: mining
			if(!nameDataTxt.getText().equals("playTennis") && !nameDataTxt.getText().equals("iris") && !nameDataTxt.getText().equals("missingPlay")) {
				final JPanel panel=new JPanel();
				JOptionPane.showMessageDialog(panel, "Table name NOT correct! Try with: playTennis/iris/missingPlay .","Error while typing table name",JOptionPane.ERROR_MESSAGE);
				nameDataTxt.setText("");
				minSupTxt.setText("");
				epsTxt.setText("");
				msgAreaTxt.setText(msgAreaTxt.getText()+"\nError while writing table name: "+nameDataTxt.getText()+" is NOT equal to playTenis/iris/missingPlay !");
			} else {
				out.println("2");
				out.println(nameDataTxt.getText());
				try {
					float minsup=(float)Float.parseFloat(minSupTxt.getText());
					if(minsup<0 || minsup>1) {
						throw new NoConsistentMinSupException("Invalid value: "+minsup+"!");
					} else {
						out.println(minSupTxt.getText());
						float epsilon=(float)Float.parseFloat(epsTxt.getText());
						if(epsilon<0 || epsilon>1) {
							throw new NoConsistentEpsException("Invalid value: "+minsup+"!");
						} else {
							out.println(epsTxt.getText());
							nameDataTxt.setEditable(true);//it is also possible to...
							minSupTxt.setEditable(true);//... re-launch the "run" button and...
							epsTxt.setEditable(true);//...see the new frequent and closed patterns (instead of closing and re-launching the applet)!
							int i=1;
							String input=in.readLine();
							while(!input.equals("STOP")){//frequent patterns
								patternsAreaTxt.setText(patternsAreaTxt.getText()+"\n"+i+") "+input);
								input=in.readLine();
								i++;
							}
							patternsAreaTxt.setText(patternsAreaTxt.getText()+"\n-----------------------------------------------------------\n");
							input=in.readLine();
							while(!input.equals("STOP")) {//closed patterns
								patternsAreaTxt.setText(patternsAreaTxt.getText()+"\n"+input);
								input=in.readLine();
							}
							patternsAreaTxt.setText(patternsAreaTxt.getText()+"\n"+in.readLine());//returns "Accomplished!"
						}
					}
				} catch(NoConsistentMinSupException ncmse) {
					msgAreaTxt.setText(msgAreaTxt.getText()+"\nError while typing support: "+minSupTxt.getText()+" is NOT in [0,1] !");
					final JPanel panel=new JPanel();
					JOptionPane.showMessageDialog(panel, "Support NOT correct! It must be in [0,1].","Error while typing support",JOptionPane.ERROR_MESSAGE);
					nameDataTxt.setText("");
					minSupTxt.setText("");
					epsTxt.setText("");
				} catch(NoConsistentEpsException ncee) {
					msgAreaTxt.setText(msgAreaTxt.getText()+"\nError while typing epsilon: "+epsTxt.getText()+" is NOT in [0,1] !");
					final JPanel panel=new JPanel();
					JOptionPane.showMessageDialog(panel, "Epsilon NOT correct! It must be in [0,1].","Error while typing epsilon",JOptionPane.ERROR_MESSAGE);
					nameDataTxt.setText("");
					minSupTxt.setText("");
					epsTxt.setText("");
				}
			}
		} else {
			out.println("1");//reading patterns from file
			String from_file=in.readLine();
			if(from_file==null) {
				msgAreaTxt.setText(msgAreaTxt.getText()+"\nError while reading from file: it could be empty!");
				final JPanel panel=new JPanel();
				JOptionPane.showMessageDialog(panel, "Problems with file!","Error while reading from file",JOptionPane.ERROR_MESSAGE);
				nameDataTxt.setText("");
				minSupTxt.setText("");
				epsTxt.setText("");
				patternsAreaTxt.setText(patternsAreaTxt.getText()+"\n----------------------------------------\nERROR: SEE BELOW FOR FURTHER INFORMATIONS\n----------------------------------------");
			} else {
				while(!from_file.equals("STOP")) {//closed patterns
					patternsAreaTxt.setText(patternsAreaTxt.getText()+"\n"+from_file);
					from_file=in.readLine();
				}
			}
			
		}
		db.setSelected(true);
		patternsAreaTxt.setText(patternsAreaTxt.getText()+"\n-------------------------------------------------------------------------------------------------------------");
		nameDataTxt.setText("");
		minSupTxt.setText("");
		epsTxt.setText("");
	}
	
	public static void main(String[] args) {
		Discovery d=new Discovery();
	}
	
	/**
	 * This class is a personalized exception class that extends the basic Exception class:
	 * it maganes the case epsilon value is not between 0 and 1 (so epsilon isn't in [0,1])
	 * @author manos
	 *
	 */
	class NoConsistentEpsException extends Exception {
		/**
		 * 
		 */
		private static final long serialVersionUID = 8058711190646689256L;

		/**
		 * Overridden constructor of the generic Exception native class
		 */
		public NoConsistentEpsException() {
			super();
		}

		/**
		 * Overridden constructor with a string parameter: this is a contextual message
		 * @param msg an arbitrary message
		 */
		public NoConsistentEpsException(String msg) {
			super(msg);
		}
	}

	/**
	 * This is a personalized exception used to handle a non-consistent range of values hired by a continuous attribute
	 * @author manos
	 *
	 */
	class NoConsistentMinSupException extends Exception{
		/**
		 * 
		 */
		private static final long serialVersionUID = 8742231465673736548L;
		/**
		 * Overridden constructor of the generic Exception native class
		 */
		public NoConsistentMinSupException() {
			super();
		}
		/**
		 * Overridden constructor with a string parameter: this is a contextual message
		 * @param msgan arbitrary message
		 */
		public NoConsistentMinSupException(String msg) {
			super(msg);
		}

	}

	/**
	 * This exception class manages the case in which a server_test connection goes wrong
	 * @author manos
	 *
	 */
	class ServerException extends Exception {
		
		private static final long serialVersionUID = 3447838903894882248L;

		/**
		 * Normal constructor to throw exception
		 */
		public ServerException() {
			super();
		}
		
		/**
		 * Second type of constructor that takes a string as an informative message about context
		 * @param msg a user typed message
		 */
		public ServerException(String msg) {
			super(msg);
		}
	}

}
