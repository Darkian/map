package server_test;

import java.io.*;
import java.net.*;
/**
 * This class manages connections to a server_test in order to accomplish frequent and closed patterns discovery
 * @author manos
 *
 */
public class MultiServer extends Thread{
	
	public static final int PORT=8080;
	
	/**
	 * This constructor starts a thread printing a message
	 * @throws IOException the constructor doesn't catch this type of exception
	 */
	public MultiServer() throws IOException {
		System.out.println("Server started!");
		start();
	}
	
	/**
	 * The server_test starts a thread that accepts all connections from multiple clients; in particular,
	 * for each client it starts a dedicated session (see ServerOneClient.java)
	 */
	public void run() {
		int clients_connected=0;
		ServerSocket ss=null;
		try {
			ss=new ServerSocket(PORT);
			while(true) {
				Socket s=ss.accept();
				try {
					new ServerOneClient(s);
					++clients_connected;
					System.out.println("Clients connected so far: "+clients_connected);
				} catch(IOException e) {
					e.printStackTrace(System.err);
					s.close();
				}
			}
		} catch(IOException se) {
			se.printStackTrace(System.err);
		}
		try {
			ss.close();
		} catch (IOException e) {
			e.printStackTrace(System.err);
		}
	}
	
	/**
	 * This is the main method (nothing special...)
	 * @param args an array of all parameters set by the IDE
	 * @throws IOException an exception generated if something, in the constructor, goes wrong
	 */
	public static void main(String[] args) throws IOException {
		MultiServer ms=new MultiServer();
	}
}
