package server_test;

import java.io.*;
import java.net.*;
import java.util.LinkedList;
import data.*;
import database.*;
import mining.*;

/**
 * This class manages each connection from the MultiServer class using threads
 * @author manos
 *
 */
public class ServerOneClient extends Thread {

	private Socket socket;

	private BufferedReader in;

	private PrintWriter out;

	private ClosedPatternArchive cpa;

	/**
	 * This constructor validates a connection from a single client_test
	 * @param s a socket (so, a communication channel) to use establish a dialog between server_test and client_test
	 * @throws IOException anyway, something could go wrong
	 */
	public ServerOneClient(Socket s) throws IOException{
		socket=s;
		in=new BufferedReader(new InputStreamReader(socket.getInputStream()));
		out=new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
		start();
	}

	/**
	 * Thread method to effectively start the thread
	 */
	public void run(){
		try {
			while(true) {
				System.out.println("Waiting for answer...");
				String getInfo=in.readLine();
				switch(getInfo) {
				case "1"://getting file info
					System.out.println("Case 1");
					cpa=(ClosedPatternArchive)ClosedPatternMiner.load();
					for(FrequentPattern fpJ:cpa.allKeys()) {
						out.println(fpJ.toString()+" ---> "+cpa.getRedundants(fpJ).toString());
						out.flush();
					}
					out.println("STOP");
					break;
				case "2"://starting closed patterns discovery
					System.out.println("Case 2");
					try {
						String table=in.readLine();
						System.out.println(table);
						Data d=new Data(table);
						String minS=in.readLine();
						float minSup=Float.parseFloat(minS);
						String eps=in.readLine();
						float epsilon=Float.parseFloat(eps);
						System.out.println(minSup+", "+epsilon);
						System.out.println("Done almost...");
						LinkedList<FrequentPattern> outFP;
						try {
							outFP=FrequentPatternMiner.frequentPatternDiscovery(d, minSup);
						} catch(Exception e) {
							outFP=null;
						}
						System.out.println("Getting frequent patterns...");
						for(FrequentPattern fp:outFP)
							out.println(fp.toString());
						out.println("STOP");
						cpa=ClosedPatternMiner.closedPatternDiscovery(outFP, epsilon);
						System.out.println("Getting closed patterns...");
						out.flush();
						for(FrequentPattern fp:cpa.allKeys())
							out.println(fp.toString()+": "+cpa.getRedundants(fp));
						out.println("STOP");
						out.flush();
						ClosedPatternMiner.save(cpa);
						out.println("Accomplished!");
					} catch (NoValueException | DatabaseConnectionException | EmptySetException e) {
						e.printStackTrace(System.err);
					}
					break;
				}
			}
		} catch (IOException e) {
			e.printStackTrace(System.err);
		}
	}

}
