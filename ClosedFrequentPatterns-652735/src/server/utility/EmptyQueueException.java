package utility;
/**
 * This is a personalized exception used to manage an empty queue at run-time
 * @author manos
 *
 */
public class EmptyQueueException extends RuntimeException {
	
	private static final long serialVersionUID = -770894462014671253L;
	/**
	 * Overridden constructor of the RuntimeException native class
	 */
	EmptyQueueException() {
		super();
	}
	/**
	 * Overridden constructor with a string parameter: this is a contextual message
	 * @param msg an arbitrary message 
	 */
	EmptyQueueException(String msg) {
		super(msg);
	}
}
