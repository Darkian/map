package data;

import java.io.Serializable;

/**
 * This class manages the idea of an attribute
 * @author manos
 *
 */
public abstract class Attribute implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6081590835771025609L;
	/**
	 * This local state variable is the name of the attribute
	 */
	protected String name;
	/**
	 * This integer is an alias in the "attributes' domain"
	 */
	protected int index;
	
	/**
	 * Constructor
	 * @param n the name of the attribute
	 * @param i and its index
	 */
	public Attribute(String n, int i) {
		name=n;
		index=i;
	}
	
	/**
	 * This method returns the name of the attribute
	 * @return the name of the attribute
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * This method returns the index
	 * @return its index
	 */
	public int getIndex() {
		return index;
	}
	
	/**
	 * Support method to better visualize the name of the attribute (overriden)
	 */
	public String toString() {
		return name;
	}
	
}
