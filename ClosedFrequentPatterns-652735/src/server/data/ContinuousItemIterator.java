package data;
import java.io.Serializable;
import java.util.*;

/**
 * This class manages an iteration in a range of float values
 * @author manos
 *
 */
public class ContinuousItemIterator implements Iterator<Float>, Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7211712993301996990L;
	/**
	 * Minimum range value
	 */
	private float min;
	/**
	 * Maximum range value
	 */
	private float max;
	/**
	 * An index used to move into each sub-interval
	 */
	private int j=0;
	/**
	 * Number of sub-intervals with same length
	 */
	private int numValues;
	
	/**
	 * This constructor set the minimum and maximum value and the number of total sub-sets with same length
	 * @param min the minimum range value
	 * @param max the maximum range value
	 * @param numValues the number of total sub-intervals with the same length
	 */
	public ContinuousItemIterator(float min, float max, int numValues) {
		this.min=min;
		this.max=max;
		this.numValues=numValues;
	}
	
	/**
	 * This method is specified by the interface Iterator and check whenever there's another subset
	 */
	public boolean hasNext() {
		if(j<=numValues-1) return true; else return false;
	}
	
	/**
	 * This method is specified by the interface Iterator and returns the minimum value related to the next subset
	 */
	public Float next() {
		if(j==0) {
			j++;
			return new Float(min+numValues);
		} else {
			if(hasNext()) {
				j++;
				Float res=new Float(min+(j*(numValues-1)));
				return res;
			} else {
				return new Float(min+(j*(numValues-1)));
			}
		}
	}
	
	/**
	 * This method is specified by the interface Iterator and removes a subset of elements (NOT IMPLEMENTED!)
	 */
	public void remove() {
		
	}
}
