package data;

/**
 * This is a personalized exception used to manage an empty data population
 * @author manos
 *
 */
public class EmptySetException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3465505394053469761L;
	/**
	 * Overridden constructor of the generic Exception native class
	 */
	public EmptySetException() {
		super();
	}
	/**
	 * Overridden constructor with a string parameter: this is a contextual message
	 * @param msg an arbitrary message
	 */
	public EmptySetException(String msg) {
		super(msg);
	}

}
