package data;
import java.io.Serializable;
import java.util.*;
/**
 * A particular case of an Attribute: a continuous attribute (only numeric values interpreted as minimum and maximum range values)
 * @author manos
 *
 */
public class ContinuousAttribute extends Attribute implements Iterable<Float>, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3818247460944913935L;
	/**
	 * The minimum of a range of possible numbers
	 */
	private float min;
	/**
	 * The maximum of a range of possible numbers
	 */
	private float max;
	
	/**
	 * This is the constructor of this class:it says that an attribute is continuous ad it associates an index
	 * @param n the name of the attribute
	 * @param i its index
	 * @param MIN minimum representable value
	 * @param MAX maximum representable value
	 */
	public ContinuousAttribute(String n, int i, float MIN, float MAX) {
		super(n, i);
		min=MIN;
		max=MAX;
	}
	
	/**
	 * This method returns the minimum value of the interval
	 * @return minimum range value
	 */
	public float getMin() {
		return min;
	}
	
	/**
	 * This method returns the maximum value of the interval
	 * @return maximum range value
	 */
	public float getMax() {
		return max;
	}
	
	/**
	 * This method returns an iterator to loop on the interval
	 */
	public Iterator<Float> iterator() {
		return new ContinuousItemIterator(min, max, 5);
	}
}
