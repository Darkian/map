package data;

import java.io.Serializable;

/**
 * A particular case of an Attribute: a discrete attribute (that means no numbers, just words)
 * @author manos
 *
 */
public class DiscreteAttribute extends Attribute implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8763651611536112268L;
	/**
	 * List of possible values hired by an attribute
	 */
	private String values[];

	/**
	 * Thi is the constructor method
	 * @param n the name of the attribute (used in super-constructor)
	 * @param i its index (used in super-constructor)
	 * @param vals list of possible values
	 */
	public DiscreteAttribute(String n, int i, String vals[]) {
		super(n, i);
		values=vals;
	}
	
	/**
	 * This method returns the number of distinct values of the attribute
	 * @return number of distinct values
	 */
	public int getNumberOfDistinctValues() {
		return values.length;
	}
	
	/**
	 * This method receives an index and returns its value in the values' list
	 * @param i index to check value
	 * @return its value in values' list
	 */
	public String getValue(int i) {
		return values[i];
	}
	
}
