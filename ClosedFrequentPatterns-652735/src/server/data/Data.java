package data;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import database.DBAccess;
import database.DatabaseConnectionException;
import database.NoValueException;
import database.QUERY_TYPE;
import database.TableSchema;
import database.Table_Data;
import database.Table_Data.TupleData;

/**
 * This is a core class: it manages all the transaction of a database seen as classes mentioned so far
 * @author manos
 *
 */
public class Data {
	/**
	 * A matrix indexed by rows(number of tuples) and columns(number of distinct attributes)
	 */
	private Object data[][];
	/**
	 * The number of tuples
	 */
	private int numberOfExamples;
	/**
	 * The list of the attributes
	 */
	private List<Attribute> attributeSet=new LinkedList<Attribute>();
	
	/**
	 * This constructor retrieves informations by querying a table of a MySQL database; the table is provided by the user
	 * @param tableName table's name to query
	 * @throws NoValueException if a value isn't specified in a tuple
	 * @throws DatabaseConnectionException if something goes wrong with accessing MySQL database
	 * @throws EmptySetException  if the table in the DB doesn't contains any tuples
	 */
	public Data(String tableName) throws NoValueException, DatabaseConnectionException, EmptySetException {
		try {
			Table_Data tb=new Table_Data();
			DBAccess.initConnection();
			ArrayList<TupleData> al=(ArrayList<TupleData>) tb.getTransazioni(tableName);//get all transactions in tableName
			if(al.size()==0) throw new EmptySetException("No tuples!");
			numberOfExamples=al.size();
			TableSchema tS=new TableSchema(tableName);//getting schema informations about tableName
			int numT=tS.getNumberOfAttributes();
			for(int i=0;i<numT;i++) {//looping on each transaction
				if(!tS.getColumn(i).isNumber()) {//NOT A NUMBER: so it will be a DiscreteAttribute
					String colName=tS.getColumn(i).getColumnName();//takes column's name...
					ArrayList<Object> colValues=(ArrayList<Object>) tb.getColumnValues(tableName, tS.getColumn(i));//...and all of its values
					String[] vals=new String[colValues.size()];
					int j=0;
					for(Object o:colValues) {
						vals[j]=(String)o.toString();//filling values in String array because constructor takes String[]
						j++;
					}
					attributeSet.add(new DiscreteAttribute(colName, i, vals));
				} else if(tS.getColumn(i).isNumber()) {//IS A NUMBER: so it will be a ContinuousAttribute
					String colName=tS.getColumn(i).getColumnName();//takes column's name...
					QUERY_TYPE qt=QUERY_TYPE.MIN;
					float min=(float) tb.getAggregateColumnValue(tableName, tS.getColumn(i), qt);//getting minimum number
					qt=QUERY_TYPE.MAX;
					float max=(float) tb.getAggregateColumnValue(tableName, tS.getColumn(i), qt);//getting maximum number
					attributeSet.add(new ContinuousAttribute(colName, i, min, max));
				} else
					throw new NoValueException("No value in Data() when analyzing tuples!");
			}
			data=new Object[numberOfExamples][attributeSet.size()];
			int rows=0;
			for(TupleData tuple:al) {//for each tuple
				int columns=0;
				for(Object o:tuple.tuple) {//and for each tuple's values
					data[rows][columns]=o;
					columns++;
				}
				rows++;
			}
			DBAccess.closeConnection();
		} catch(SQLException sqle) {
			sqle.printStackTrace(System.err);
			System.out.println("SQL EXCEPTION!");
		}
	}

	/*
	 * This method returns the number of the tuples
	 */
	public int getNumberOfExamples() {
		return numberOfExamples;
	}

	/**
	 * This method returns the number of distinct attributes
	 * @return number of attributes
	 */
	public int getNumberOfAttributes() {
		return attributeSet.size();
	}

	/**
	 * This method works on the matrix returning the value in indexEx row and attrEx column
	 * @param indexEx row's index
	 * @param attrEx column's index
	 * @return the correspondent value in the matrix
	 */
	public Object getAttributeValue(int indexEx, int attrEx) {
		return data[indexEx][attrEx];
	}

	/**
	 * This method returns an attribute's name in the attributes' list
	 * @param ind index 
	 * @return the attribute in the index position
	 */
	public Attribute getAttribute(int ind) {
		return attributeSet.get(ind);
	}

	/**
	 * This method concatenates, for each tuple, the correspondent values for each attribute
	 */
	public String toString() {
		String result=new String("");
		for(int i=0;i<getNumberOfExamples();i++) {
			result= result + getAttributeValue(i,0).toString() + "," + getAttributeValue(i,1).toString() + "," + getAttributeValue(i,2).toString() + "," + getAttributeValue(i,3).toString() + "," + getAttributeValue(i,4).toString() +";\n";
		}
		return result;
	}
}
