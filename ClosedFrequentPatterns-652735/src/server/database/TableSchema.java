package database;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * This class is used to manages columns and relative types of a database
 * @author manos
 *
 */
public class TableSchema {
	/**
	 * In particular, this class manages the type of a column (string or number in general)
	 * @author manos
	 *
	 */
	public class Column {
		private String name;
		private String type;
		
		/**
		 * This constructor sets a column with a name and a type
		 * @param name a string representing the name of the column
		 * @param type a string that could be "number" or "string" only
		 */
		Column(String name, String type) {
			this.name = name;
			this.type = type;
		}
		
		/**
		 * This method returns the name of the column
		 * @return a column's name
		 */
		public String getColumnName() {
			return name;
		}
		
		/**
		 * This method returns a boolean value that is true if the type's columns value is a number; false instead
		 * @return a boolean value if the type's column is a number; false instead
		 */
		public boolean isNumber() {
			return type.equals("number");
		}

		/**
		 * This method simply concatenates the name of the column and its type of value, and returns it
		 */
		public String toString() {
			return name + ":" + type;
		}
	}

	List<Column> tableSchema = new ArrayList<Column>();

	/**
	 * This class (given by the Professor) manages the matching between SQL variables types and Java variables types
	 * @param tableName the name of the table
	 * @throws SQLException a particular exception generated if something goes wrong with SQL database
	 */
	public TableSchema(String tableName) throws SQLException {
		HashMap<String, String> mapSQL_JAVATypes = new HashMap<String, String>();
		// http://java.sun.com/j2se/1.3/docs/guide/jdbc/getstart/mapping.html
		mapSQL_JAVATypes.put("CHAR", "string");
		mapSQL_JAVATypes.put("VARCHAR", "string");
		mapSQL_JAVATypes.put("LONGVARCHAR", "string");
		mapSQL_JAVATypes.put("BIT", "string");
		mapSQL_JAVATypes.put("SHORT", "number");
		mapSQL_JAVATypes.put("INT", "number");
		mapSQL_JAVATypes.put("LONG", "number");
		mapSQL_JAVATypes.put("FLOAT", "number");
		mapSQL_JAVATypes.put("DOUBLE", "number");
		Connection con = DBAccess.getConnection();
		DatabaseMetaData meta = con.getMetaData();
		ResultSet res = meta.getColumns(null, null, tableName, null);
		while (res.next()) {
			if (mapSQL_JAVATypes.containsKey(res.getString("TYPE_NAME")))
				tableSchema.add(
						new Column(res.getString("COLUMN_NAME"), mapSQL_JAVATypes.get(res.getString("TYPE_NAME"))));
		}
		res.close();
	}

	/**
	 * This method returns the number of attributes contained in the table schema
	 * @return number of attributes in a certain DB table
	 */
	public int getNumberOfAttributes() {
		return tableSchema.size();
	}
	
	/**
	 * This method takes an integer as an index and returns a column in the list of columns
	 * @param index a generic index 
	 * @return a column (so, a Column object)
	 */
	public Column getColumn(int index) {
		return tableSchema.get(index);
	}

}
