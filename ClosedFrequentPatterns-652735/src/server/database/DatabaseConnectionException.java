package database;

/**
 * This class exception manages the failure in loading DBMS driver
 * @author manos
 *
 */
public class DatabaseConnectionException extends Exception {
	
	private static final long serialVersionUID = 8846864008134562333L;
	
	/**
	 * This constructor calls it super-constructor for default exception throwing
	 */
	public DatabaseConnectionException() {
		super();
	}
	
	/**
	 * This overloaded constructor takes in input a string message that is a description of the situation
	 * @param msg a string information
	 */
	public DatabaseConnectionException(String msg) {
		super(msg);
	}
}
