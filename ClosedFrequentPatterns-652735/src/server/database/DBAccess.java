package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBAccess {
	private static final String DRIVER_CLASS_NAME = "org.gjt.mm.mysql.Driver";
	private static final String DBMS = "jdbc:mysql";
	private static final String SERVER = "localhost";
	private static final int PORT = 3306;
	private static final String DATABASE = "CFP17_652735";
	private static final String USER_ID = "manosperta";
	private static final String PASSWORD = "CFP17password";
	private static Connection conn;
	
	public static void initConnection() throws DatabaseConnectionException {
		String connectionString = DBMS+"://" + SERVER + ":" + PORT + "/" + DATABASE;
		try {
			Class.forName(DRIVER_CLASS_NAME).newInstance();
			conn = DriverManager.getConnection(connectionString, USER_ID, PASSWORD);
			if(conn==null) throw new DatabaseConnectionException("Impossible to access DB!");
		} catch (Exception ex) {
			System.out.println("Impossible finding Driver: " + DRIVER_CLASS_NAME);
		}
	}
	
	public static Connection getConnection() {
		return conn;
	}
	
	public static void closeConnection() throws SQLException {
		conn.close();
	}
}
