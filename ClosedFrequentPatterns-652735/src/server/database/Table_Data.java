package database;

import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import database.TableSchema.Column;

/**
 * This class manages a database query and all tuples that it can satisfy
 * @author manos
 *
 */
public class Table_Data {
	/**
	 * Nested class used to manage the results (so, tuples) of a query
	 * @author manos
	 *
	 */
	public class TupleData {
		/**
		 * List of all tuples
		 */
		public List<Object> tuple=new ArrayList<Object>();
		
		/**
		 * This method simply concatenates each item of a tuple in a string
		 */
		public String toString(){
			String value="";
			Iterator<Object> it= tuple.iterator();
			while(it.hasNext())
				value+= (it.next().toString() +" ");
			return value;
		}
	}
	
	/**
	 * This method takes in input a string, that is a table name, and returns all the tuples of that table
	 * @param table a table's name
	 * @return a list of all tuples
	 * @throws SQLException handled to manage exception
	 * @throws NoValueException no value for temperature
	 */
	public List<TupleData> getTransazioni(String table) throws SQLException, NoValueException {
		List<TupleData> listTP=new ArrayList<TupleData>();
		TableSchema tb=new TableSchema(table);
		Statement statement=(Statement) DBAccess.getConnection().createStatement();
		ResultSet rs=((Statement) statement).executeQuery("SELECT * FROM "+table+";");
		while(rs.next()) {
			TupleData tp=new TupleData();
			for(int i=1;i<=tb.getNumberOfAttributes();i++) {
				if(rs.getObject(i) instanceof Float) {
					tp.tuple.add((float)rs.getFloat(i));
				} else if(rs.getObject(i) instanceof String) {
					tp.tuple.add(rs.getString(i));
				} else
					throw new NoValueException();
			}
			listTP.add(tp);
		}
		rs.close();
		statement.close();
		return listTP;
	}
	
	/**
	 * This method returns a list of all values assumed by a specified column of a specified table
	 * @param table a table's name
	 * @param column a column's name in the table
	 * @return a list of all values of that column
	 * @throws SQLException handled to manage exceptions
	 */
	public List<Object> getColumnValues(String table, Column column) throws SQLException {
		List<Object> values=new ArrayList<Object>();
		Statement statement=(Statement) DBAccess.getConnection().createStatement();
		ResultSet result=((java.sql.Statement)statement).executeQuery("SELECT  DISTINCT "+column.getColumnName()+" FROM "+table+";");
		while(result.next())
			values.add(result.getObject(column.getColumnName()));
		result.close();
		statement.close();
		return values;
	}
	
	/**
	 * This method queries a certain table in a certain column (all passed in input) and determines the aggregate values, that are
	 * so returned 
	 * @param table a table's name
	 * @param column a colimn's name into the table
	 * @param aggregate an enumeration of minimum and maximum
	 * @return the aggregation value
	 * @throws SQLException handled to manage exceptions
	 * @throws NoValueException handled because of a null value in the column cell
	 */
	public Object getAggregateColumnValue(String table, Column column, QUERY_TYPE aggregate) throws SQLException, NoValueException {
		Statement statement=(Statement)DBAccess.getConnection().createStatement();
		ResultSet r=((Statement)statement).executeQuery("SELECT "+aggregate.toString().toUpperCase()+"("+column.getColumnName()+") AS "+aggregate.toString().toLowerCase()+" FROM "+table);
		float res=0;
		while(r.next())
			res=(float)r.getFloat(aggregate.toString().toLowerCase());
		r.close();
		statement.close();
		return res;
	}
}
