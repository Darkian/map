package database;

/**
 * This class manages the aggregation of a query
 * @author manos
 *
 */
public enum QUERY_TYPE {
	MIN, MAX;
}
