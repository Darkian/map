package database;

/**
 * This class manages the case in which there's a null continuous item's value
 * @author manos
 *
 */
public class NoValueException extends Exception {
	
	private static final long serialVersionUID = -6614880170410769343L;

	/**
	 * This contructor calls its super-constructor for default exception throwing
	 */
	public NoValueException() {
		super();
	}
	
	/**
	 * This overloaded constructor takes in input a string that is a description of the situation
	 * @param msg a string that describes what happened
	 */
	public NoValueException(String msg) {
		super(msg);
	}
}
