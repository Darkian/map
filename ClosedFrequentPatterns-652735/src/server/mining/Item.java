package mining;
import java.io.Serializable;

import data.Attribute;

/**
 * This class is used to manage attributes' types of a database.
 * @author manos
 *
 */
public abstract class Item implements Serializable {
	
	private static final long serialVersionUID = -2087817034605322504L;
	
	protected Attribute attribute;
	
	protected Object value;
	
	Item() {}
	/**
	 * This constructor sets the attribute to the attribute passed in input and a value
	 * @param a attribute to set
	 * @param o a generic value
	 */
	public Item(Attribute a, Object o) {
		attribute=a;
		value=o;
	}
	
	/**
	 * This method returns the local state attribute
	 * @return the name of the attribute
	 */
	public Attribute getAttribute() {
		return attribute;
	}
	
	/**
	 * This method returns the real value of the attribute
	 * @return value of the attribute
	 */
	public Object getValue() {
		return value;
	}
	
	/**
	 * This is an abstract method
	 * @param v a generic object
	 * @return a boolean value
	 */
	public abstract boolean checkItemCondition(Object v);
	
	/**
	 * This method simply make a attribute's name=value as a string
	 */
	public String toString() {
		return "<" + attribute.toString() + "="
				+ value.toString() + ">";
	}
}
