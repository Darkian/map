package mining;
import java.io.Serializable;
import java.util.*;

/**
 * This class manages the patterns that are involved in the data mining algotithm.
 * @author manos
 *
 */
public class FrequentPattern implements Iterable<Item>, Comparable<FrequentPattern>, Serializable {
	
	private transient static final long serialVersionUID = 2988362803139085465L;
	/**
	 * Array of all patterns
	 */
	private List<Item> fp;
	/**
	 * This local state variable stores the value of the support
	 */
	private float support;
	
	/**
	 * This constructor creates an empty list
	 */
	public FrequentPattern() {
		fp=new ArrayList<Item>();
	}
	
	/**
	 * This method generates a new list of patterns by adding the item gived in input
	 * @param it Item to be added
	 */
	public void addItem(Item it) {
		fp.add(it);
	}
	
	/**
	 * This method returns the item value in the array by passing to it an index.
	 * @param index an integer used to access information in the list
	 * @return the item value in thah position
	 */
	public Item getItem(int index) {
		return fp.get(index);
	}
	
	/**
	 * This method simply returns the support.
	 * @return support
	 */
	public float getSupport() {
		return support;
	}
	
	/**
	 * This method returns the length of the list used to store patterns
	 * @return list's length
	 */
	public int getPatternLength() {
		return fp.size();
	}
	
	/**
	 * This method set the support at a user value.
	 * @param s new input value provided by the user
	 */
	public void setSupport(float s) {
		support=s;
	}
	
	/**
	 * This method returns a concatenation of all items in the pattern's list.
	 */
	public String toString(){
		String value="";
		for(Item j:fp)
			value+=j.toString() + " AND ";
		if(fp.size()>0)
			value+="["+support+"]";
		return value;
	}
	
	/**
	 * An iterator for this class
	 */
	public Iterator<Item> iterator() {
		return fp.iterator();
	}
	
	/**
	 * This nested class manages iterators for FrequentPattern.java class
	 * @author manos
	 *
	 */
	class PatternIterator implements Iterator<Item> {
		private Iterator<Item> i=fp.iterator();
		public boolean hasNext() {
			return i.hasNext();
		}
		
		public Item next() {
			return i.next();
		}
		
		public void remove() {
			i.remove();
		}
	}
	
	/**
	 * This method specializes its abstract method derived from Comparable interface: it returns 
	 * 0 if implicit parameter has the same elements as the passed list,
	 * -1 if the implicit parameter has less elements than the passed one,
	 * +1 if the implicit parameter has more elements than the passed one.
	 */
	public int compareTo(FrequentPattern fp) {
		if(getPatternLength()==fp.getPatternLength())
			return 0;
		else {
			if(getPatternLength()<fp.getPatternLength())
				return -1;
			else 
				return +1;
		}
	}
	
	public ArrayList<Item> getItemList() {
		ArrayList<Item> ref=new ArrayList<Item>();
		ref=(ArrayList<Item>)fp;
		return ref;
	}
}
