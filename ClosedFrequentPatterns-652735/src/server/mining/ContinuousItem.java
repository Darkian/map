package mining;

import java.io.Serializable;
import data.*;

public class ContinuousItem extends Item implements Serializable{
	
	private transient static final long serialVersionUID = 5545820156301855261L;
	/**
	 * This field manages the inferior range value
	 */
	private float inf;
	/**
	 * This field manages the superior range value
	 */
	private float sup;
	
	/**
	 * This constructor takes a ContinuousAttribute and an Inverval value and calls its super-constructor
	 * in order to set the attributes to the passed values
	 * @param ca a continuous attribute
	 * @param value a range of values it can assume
	 */
	public ContinuousItem(ContinuousAttribute ca, Interval value) {
		super(ca,value);
		this.inf=ca.getMin();
		this.sup=ca.getMax();
	}
	
	/**
	 * This method is an implementation of the abstract one given in superclass:
	 * it check if the value passed as a parameter is a float or an Interval:
	 * in the first case, it checks if value is between minimum and maximum range values;
	 * in the second case, it checks if the minimum Interval value is greater or equals than the private attribute field
	 * and if the maximum Interval value is less equal or equal than the private attribute field
	 */
	public boolean checkItemCondition(Object value) {
		if(value instanceof Interval)
			return(((Interval)value).getInf()>=inf && ((Interval)value).getSup()<=sup);
		else if(value instanceof Float)
			return((float)value>=inf && (float)value<=sup);
		else return false;
	}
	
	/**
	 * This method returns a concatenation of the attribute and its interval as a string
	 */
	public String toString() {
		return "<"+super.attribute.toString()+"="+super.value.toString()+">";
	}
}
