package mining;
import java.io.*;
import java.util.*;

/**
 * This class manages the discovery of closed patterns (and, eventually, redundant patterns) staring by the frequent patterns found before.
 * @author manos
 *
 */
public class ClosedPatternMiner implements Serializable, Irrilevancy {
	
	private transient static final long serialVersionUID = 993402617512274652L;

	/**
	 * While scanning the list of frequent patterns, it verify whenever a pattern satisfies both closed conditions.
	 * Please note: the frequent redundant pattern and closed pattern association is not exclusive.
	 * @param outFP list of frequent patterns
	 * @param epsilon maximum value to compute support
	 * @return containers the set of closed patterns and related redundant patterns
	 */
	public static ClosedPatternArchive closedPatternDiscovery(LinkedList<FrequentPattern> outFP, float epsilon) {
		ClosedPatternMiner cpm=new ClosedPatternMiner();
		cpm.prunePermutations(outFP);
		cpm.pruneSingle(outFP);
		ClosedPatternArchive cpa=new ClosedPatternArchive();
		LinkedList<FrequentPattern> redundants=new LinkedList<FrequentPattern>();//it manages redundant patterns
		int maxLength, current_length;
		try {
			for(FrequentPattern fp:outFP) {
				maxLength=fp.getPatternLength();
				current_length=maxLength;
				while(current_length>1) {
					current_length--;
					for(FrequentPattern f:outFP) {//takes all frequent patterns which length is equal to current_length
						if(f.getPatternLength()==current_length) {
							if(isCoveredBySupport(fp, f, epsilon) && isCoveredByItems(fp, f)) {
								cpa.put(fp, f);
								redundants.remove(f);
								//outFP.remove(f);
							}
						}
					}
				}
			}
		} catch(ConcurrentModificationException cme) {
			System.out.println("ERROR -> ConcurrentModificationException");
			cme.printStackTrace(System.err);
		} catch(NullPointerException npe) {
			System.out.println("ERROR -> NullPointerException");
			npe.printStackTrace(System.err);
		} catch(Exception e) {
			System.out.println("ERROR -> Exception");
			e.printStackTrace(System.err);
		}
		return cpa;
	}

	/**
	 * This method manages the case in which two frequent patterns have a support difference less equal than a certain epsilon
	 * @param current the current frequent pattern
	 * @param examined the examined frequent pattern
	 * @param epsilon maximum delta value
	 * @return a boolean value that is true if the distance is less equal than epsilon; false if not
	 */
	private static boolean isCoveredBySupport(FrequentPattern current, FrequentPattern examined, float epsilon) {
		return (Math.abs(current.getSupport()-examined.getSupport())<=epsilon);
	}

	/**
	 * Check if examined patters are contained in current patterns (subset)
	 * @param current candidate closed pattern
	 * @param examined candidate redundant pattern
	 * @return the boolean truth value
	 */
	private static boolean isCoveredByItems(FrequentPattern current, FrequentPattern examined) {
		if(examined.getPatternLength()>=current.getPatternLength())
			return false;
		else {
			boolean subset=true;
			Iterator<Item> i;
			for(i=examined.iterator();i.hasNext(); ) {
				Item temp=i.next();
				boolean found=false;
				for(Iterator<Item> j=current.iterator(); j.hasNext() && found==false; ) {
					Item temp1=j.next();
					if(temp instanceof DiscreteItem && temp1 instanceof DiscreteItem) {
						if(temp1.checkItemCondition(temp.getValue()))
							found=true;
					}
					else if(temp instanceof ContinuousItem && temp1 instanceof ContinuousItem) {
						if(temp1.checkItemCondition(temp.getValue()))
							found=true;
					}
				}
				if(found==false) {
					subset=false;
					break;
				}
			}
			return subset;
		}
	}

	/**
	 * This method returns the maximum pattern's length of a patterns' list.
	 * @param outFP linked list that contains all the frequent patterns
	 * @return the maximum length of a certain frequent pattern
	 */
	private static int getMaxLength(LinkedList<FrequentPattern> outFP) {
		int max_length=0;
		for(FrequentPattern fp:outFP) {
			if(fp.getPatternLength()>max_length)
				max_length=fp.getPatternLength();
		}
		return max_length;
	}

	/**
	 * This method is used because the final user could save the closed pattern archive in order to
	 * not re-do the mining whenever he/she launches again the executable
	 * @param csa the closed pattern archive to save onto a file
	 */
	public static void save(ClosedPatternArchive csa) {
		try {
			FileOutputStream outFile=new FileOutputStream("archive.txt");
			ObjectOutputStream outStream=new ObjectOutputStream(outFile);
			outStream.writeObject(csa);
			outFile.close();
			outStream.close();
		} catch(IOException ioe) {
			ioe.printStackTrace(System.err);
		}
	}

	/**
	 * This method is used whenever the final user save onto a file the closed pattern archive and then he/she
	 * wants to read it in order to not re-do the mining
	 * @return the whole closed pattern archive
	 */
	public static ClosedPatternArchive load() {
		try {
			FileInputStream in=new FileInputStream("archive.txt");
			ObjectInputStream inStream=new ObjectInputStream(in);
			ClosedPatternArchive result=(ClosedPatternArchive) inStream.readObject();
			inStream.close();
			in.close();
			return result;
		} catch(FileNotFoundException fnfe) {
			fnfe.printStackTrace(System.err);
			return new ClosedPatternArchive();
		} catch (ClassNotFoundException cnfe) {
			cnfe.printStackTrace(System.err);
			return new ClosedPatternArchive();
		} catch(IOException ioe) {
			ioe.printStackTrace(System.err);
			return new ClosedPatternArchive();
		}
	}

	/**
	 * This method is an implementation of the abstract one, and deletes all item sets that are equals and with identical support
	 * @param outFP list of all frequent patterns
	 */
	public void prunePermutations(LinkedList<FrequentPattern> outFP) {
		try {
			for(Iterator<FrequentPattern> fp=outFP.iterator();fp.hasNext(); ) {
				if(isPresent(fp.next(), outFP)) {
					fp.remove();
				}
			}
		} catch(Exception e) {
			e.printStackTrace(System.err);
		}
	}

	/**
	 * This method is an implementation of the abstract one, and deletes all patterns with 1-length
	 * @param outFP list of all frequent patterns
	 */
	public void pruneSingle(LinkedList<FrequentPattern> outFP) {
		try {
			for(Iterator<FrequentPattern> fp=outFP.iterator();fp.hasNext(); ) {
				if(fp.next().getPatternLength()==1)
					fp.remove();
			}
		} catch(Exception e) {
			e.printStackTrace(System.err);
		}
	}

	/**
	 * This method is used to verify whenever a frequent patterns' set contains (somewhere) a duplicate of a certain frequent pattern value 
	 * @param fp the frequent patterns' list
	 * @param outputFP a list containing all the potential patterns
	 * @return a boolean value that is true if a duplicate of a certain pattern is found somewhere; false if not
	 */
	private static boolean isPresent(FrequentPattern fp, LinkedList<FrequentPattern> outputFP){
		boolean uguali=false;
		FrequentPattern temp;
		for(Iterator<FrequentPattern> it=outputFP.iterator();it.hasNext() && uguali==false; ) {
			uguali=true;
			temp=it.next();
			//if the patterns' lengths are different, so patterns would be different too
			if(temp.getPatternLength()!=fp.getPatternLength())
				uguali=false;
			else {
				for(int i=0;i<temp.getPatternLength() && uguali==true; i++) {
					boolean trovato=false;
					for(int j=0;j<fp.getPatternLength() && trovato==false; j++) {
						if(temp.getItem(i) instanceof DiscreteItem && fp.getItem(j) instanceof DiscreteItem) {
							if(temp.getItem(i).checkItemCondition(fp.getItem(j).getValue()))//if(temp.getItem(i).getValue().equals(fp.getItem(j).getValue()))
								trovato=true;
						}
						else if(temp.getItem(i) instanceof ContinuousItem && fp.getItem(j) instanceof ContinuousItem) {
							if(temp.getItem(i).checkItemCondition(fp.getItem(j).getValue()))
								trovato=true;
						}
					}
					//if a certain value isn't find in the pattern, it would mean that patterns are different 
					if(trovato==false || temp==fp)
						uguali=false;
				}
			}
		}
		return uguali;
	}
}
