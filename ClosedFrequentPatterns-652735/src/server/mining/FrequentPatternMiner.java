package mining;

import data.*;
import data.Attribute;
import data.Data;
import data.DiscreteAttribute;
import utility.*;
import utility.Queue;

import java.util.*;

/**
 * This class performs the data mining action: it only finds out the frequent patterns with at least 1 length.
 * @author manos
 *
 */
public class FrequentPatternMiner {
	/**
	 * Returns a list of patterns with 1 length, 2 etc
	 * @param data in which there are tuples of a database
	 * @param minSup minimum support about calculating patterns
	 * @return all patterns with 1+ length
	 * @throws Exception if an attribute is not identified, this method throws an exception
	 */
	public static LinkedList<FrequentPattern> frequentPatternDiscovery(Data data, float minSup) throws Exception{
		Queue<FrequentPattern> fpQueue=new Queue<FrequentPattern>();		
		LinkedList<FrequentPattern> outputFP=new LinkedList<FrequentPattern>();
		if(data.getNumberOfExamples()==0) throw new EmptySetException("Number of examples = 0!");
		for(int i=0;i<data.getNumberOfAttributes();i++){
			Attribute currentAttribute=data.getAttribute(i);
			//check if selected attribute is discrete or continuous
			if(currentAttribute instanceof DiscreteAttribute) {
				for(int j=0;j<((DiscreteAttribute)currentAttribute).getNumberOfDistinctValues();j++){	
					DiscreteItem itemD=new DiscreteItem( 
							(DiscreteAttribute)currentAttribute, 
							((DiscreteAttribute)currentAttribute).getValue(j));
					FrequentPattern fp=new FrequentPattern();//generates a new frequent pattern with this item and calculates support
					fp.addItem(itemD);
					fp.setSupport(FrequentPatternMiner.computeSupport(data, fp));
					if(fp.getSupport()>minSup){
						outputFP.add(fp);
						fpQueue.enqueue(fp);
					}
				}
			} else if(currentAttribute instanceof ContinuousAttribute) {
				float minTemperature=((ContinuousAttribute) currentAttribute).getMin();
				for(Iterator<Float> f=((ContinuousAttribute) currentAttribute).iterator(); f.hasNext();) {
					float maxTemperature=f.next();
					Interval itv=new Interval(minTemperature,maxTemperature);
					ContinuousItem itemC=new ContinuousItem(
							(ContinuousAttribute) currentAttribute,
							((Interval)itv));
					FrequentPattern fp=new FrequentPattern();
					fp.addItem(itemC);
					fp.setSupport(FrequentPatternMiner.computeSupport(data, fp));
					if(fp.getSupport()>minSup) {
						outputFP.add(fp);
						fpQueue.enqueue(fp);
					}
					minTemperature=maxTemperature;
				}
			} else
				throw new Exception("Something went wrong in frequentPatternDiscovery!");
		}
		outputFP=expandFrequentPatterns(data,minSup,fpQueue,outputFP);
		return outputFP;
	}

	/**
	 * Returns a list with all 2+ patterns length
	 * @param data the same as above
	 * @param minSup the same as above
	 * @param fpQueue queue containing elements to be analyzed
	 * @param outputFP this list contains all the 1-length patterns
	 * @return the frequent patterns with 2+ length
	 * @throws Exception if an attribute is not identified, this method throws an exception
	 */
	private static LinkedList<FrequentPattern> expandFrequentPatterns(Data data, float minSup, Queue<FrequentPattern> fpQueue, LinkedList<FrequentPattern> outputFP) throws Exception{
		LinkedList<FrequentPattern> extra=new LinkedList<FrequentPattern>();
		while (!fpQueue.isEmpty()){
			FrequentPattern fp=(FrequentPattern)fpQueue.first();//fp to expand
			try {
				fpQueue.dequeue();
			}catch(EmptyQueueException eqe) {
				eqe.printStackTrace(System.err);
				break;
			}
			for(int i=0;i<data.getNumberOfAttributes();i++){
				boolean found=false;
				for(int j=0;j<fp.getPatternLength();j++) {//the new item should involve an attribute that is different form attributes already involved into the items of fp
					if(fp.getItem(j).getAttribute().equals(data.getAttribute(i))){//checking attributes' name
						found=true;
						break;
					}
				}
				if(!found) {//each data.getAttribute(i) is not involved into an item of fp
					if(data.getAttribute(i) instanceof ContinuousAttribute) {
						float minTemperature=((ContinuousAttribute)data.getAttribute(i)).getMin();
						for(Iterator<Float> f=((ContinuousAttribute)data.getAttribute(i)).iterator(); f.hasNext(); ) {
							float maxTemperature=f.next();
							ContinuousItem item=new ContinuousItem(
									(ContinuousAttribute)data.getAttribute(i),
									new Interval(minTemperature, maxTemperature));
							FrequentPattern newFP=FrequentPatternMiner.refineFrequentPattern(fp, item);
							newFP.setSupport(FrequentPatternMiner.computeSupport(data, newFP));
							if(newFP.getSupport()>minSup) {
								extra.add(newFP);
								fpQueue.enqueue(newFP);
							}
							minTemperature=maxTemperature;
						}
					} else if(data.getAttribute(i) instanceof DiscreteAttribute) {
						for(int k=0;k<((DiscreteAttribute)data.getAttribute(i)).getNumberOfDistinctValues();k++) {
							DiscreteItem item=new DiscreteItem(
									(DiscreteAttribute)data.getAttribute(i),
									((DiscreteAttribute)(data.getAttribute(i))).getValue(k));
							FrequentPattern newFP=FrequentPatternMiner.refineFrequentPattern(fp,item);//generate refinement
							newFP.setSupport(FrequentPatternMiner.computeSupport(data,newFP));
							if(newFP.getSupport()>minSup){
								fpQueue.enqueue(newFP);
								outputFP.add(newFP);
							}
						}
					} else 
						throw new Exception("Something went wrong in expandFrequentPatterns!");
				}
			}
		}
		return extra;
	}

	/**
	 * This method returns the number of times the frequent pattern passed in input occurs in data
	 * @param data all the transactions
	 * @param FP a frequent pattern to calculate support
	 * @return returns the support of each pattern's real value compared to each transaction
	 * @throws Exception if an attribute is not identified, this method throws an exception
	 */
	public static float computeSupport(Data data, FrequentPattern FP) throws Exception {
		int k=0;
		for(int i=0;i<data.getNumberOfExamples();i++) {
			boolean found=true;
			for(int j=0;j<FP.getPatternLength() && found;j++) {
				if(FP.getItem(j) instanceof DiscreteItem) {
					if(!(((DiscreteItem)FP.getItem(j)).checkItemCondition(data.getAttributeValue(i, FP.getItem(j).getAttribute().getIndex()))))
						found=false;
				} else if(FP.getItem(j) instanceof ContinuousItem) {
					if(!(((ContinuousItem)FP.getItem(j)).checkItemCondition(data.getAttributeValue(i, FP.getItem(j).getAttribute().getIndex()))))
						found=false;
				} else
					throw new Exception("Something went wrong in computeSupport!");
			}
			if(found)
				k++;
		}
		return (float)k/data.getNumberOfExamples();
	}

	/**
	 * Just adds the item in the frequent patterns set and returns the new set
	 * @param FP all the patterns
	 * @param item an item to be added in the patterns' set
	 * @return a new set of frequent patterns with the new item in
	 */
	public static FrequentPattern refineFrequentPattern(FrequentPattern FP, Item item){
		FrequentPattern temp=new FrequentPattern();
		for(int i=0; i<FP.getPatternLength();i++)
			temp.addItem(FP.getItem(i));
		temp.addItem(item);
		return temp;
	}
}
