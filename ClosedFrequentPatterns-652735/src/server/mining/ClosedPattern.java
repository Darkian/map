package mining;
import java.util.*;

/**
 * This class manages the case in which a frequent pattern in also closed.
 * @author manos
 *
 */
public class ClosedPattern extends FrequentPattern {
	
	private transient static final long serialVersionUID = -2560829696698851819L;
	
	/**
	 * This constructor takes as input a linked list of items and a support, and
	 * it calls its super-constructor, sets the support and add each element of the linked list in itself
	 * @param fp a list of frequent patterns
	 * @param support the support of a frequent pattern
	 */
	public ClosedPattern(LinkedList<Item> fp, float support) {
		super();
		this.setSupport(support);
		for(Item i:fp)
			this.addItem(i);
	}

}
