package mining;
import java.io.Serializable;
import java.util.*;

/**
 * This class shapes all the frequent patterns that are closed (the key value of the dictionary)
 * and the related redundant patterns (as its values).
 * @author manos
 *
 */
public class ClosedPatternArchive implements Serializable{
	/**
	 * 
	 */
	private transient static final long serialVersionUID = 4406627264702346733L;
	/**
	 * This is the real collection of closed and frequent patterns organized as a hash table
	 */
	private HashMap<FrequentPattern, TreeSet<FrequentPattern>> archive=new HashMap<FrequentPattern, TreeSet<FrequentPattern>>();
	
	/**
	 * This method adds int the archive a frequent pattern without a related specific value, if not contained yet
	 * @param fp frequent pattern to be added
	 */
	public void put(FrequentPattern fp) {
		archive.putIfAbsent(fp, null);
	}
	
	/**
	 * This method adds the frequent pattern, if it is not already present.
	 * If it is, this method will put the second parameter in the set related to the  key pattern.
	 * @param fp the dictionary key
	 * @param pattern the related value
	 */
	public void put(FrequentPattern fp, FrequentPattern pattern) {
		if(!archive.containsKey(pattern) && archive.containsKey(fp))
			archive.get(fp).add(pattern);
		else {
			archive.put(fp, new TreeSet<FrequentPattern>());
			archive.get(fp).add(pattern);
		}
	}
	
	/*
	 * 
	 * This methods returns the redundant patterns related to the key passed as a parameter.
	 * If not, returns the key.
	 */
	public TreeSet<FrequentPattern> getRedundants(FrequentPattern fp) {
		if(archive.get(fp).isEmpty()) {
			TreeSet<FrequentPattern> res=new TreeSet<FrequentPattern>();
			res.add(fp);
			return res;
		} else 
			return archive.get(fp);
	}
	
	/**
	 * This method returns a string that, for each key value, concatenates all values related to each key.
	 */
	public String toString() {
		String value="";
		for(FrequentPattern k:archive.keySet()) {
			value+=k.toString() + "\n   ---> ";
			if(archive.get(k).isEmpty()) 
				value+=" E M P T Y !";
			else {
				TreeSet<FrequentPattern> ts=getRedundants(k);
				for(FrequentPattern v:ts) {
					value+=v.toString() + ", ";
				}
			}
			value+="\n";
		}
		return value;
	}
	
	/**
	 * This method is been added by the programmer to perform client_test-server_test communication with sockets and,
	 * in particular, to manage the printing of each key and related value(s) to avoid NullPinterException
	 * @return the set of all frequent patterns: so all the frequent patterns that are keys in the hash map
	 */
	public Set<FrequentPattern> allKeys() {
		return archive.keySet();
	}
}
