/**
 * 
 */
package mining;

import java.util.LinkedList;

/** This interface will be specialized in ClosedPatternMiner and it will remove frequent patterns with 1 length and all
 * the permutations of each frequent pattern
 * @author manos
 *
 */
public interface Irrilevancy {
	/**
	 * It will remove permutations of each frequent pattern
	 * @param outFP list of all frequent patterns
	 */
	void prunePermutations(LinkedList<FrequentPattern> outFP);
	
	/**
	 * It will remove all the frequent patterns with 1 length
	 * @param outFP list of all frequent patterns
	 */
	void pruneSingle(LinkedList<FrequentPattern> outFP);
}
