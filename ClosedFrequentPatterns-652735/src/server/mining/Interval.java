package mining;

import java.io.Serializable;

/**
 * This class manages the case in which the attribute is continuous: in fact, it needs an interval of possible values
 * @author manos
 *
 */
public class Interval implements Serializable{
	
	private static final long serialVersionUID = -216304688949823495L;
	/**
	 * minimum range value
	 */
	private float inf;
	/**
	 * maximum range value
	 */
	private float sup;
	
	/**
	 * This constructor sets the minimum and maximum values of the class
	 * @param m the minimum range value
	 * @param s the maximum range value
	 */
	public Interval(float m, float s) {
		inf=m;
		sup=s;
	}

	/**
	 * This method simply returns the minimum range value
	 * @return the minimum interval value
	 */
	public float getInf() {
		return inf;
	}

	/**
	 * This method simply sets the minimum range value
	 * @param inf a float number as a minimum range value
	 */
	public void setInf(float inf) {
		this.inf = inf;
	}

	/**
	 * This method simply returns the maximum range value
	 * @return the maximum interval value
	 */
	public float getSup() {
		return sup;
	}

	/**
	 * This method simply sets the maximum interval value
	 * @param sup a float number as the maximum range value
	 */
	public void setSup(float sup) {
		this.sup = sup;
	}
	
	/**
	 * This method takes in input a float number and checks whenever this number is between the minimum and maximum interval values
	 * @param value a float number to check inclusion in the interval
	 * @return a boolean value that is true if the number is between the minimum and the maximum range values; false if not
	 */
	public boolean checkItemInclusion(float value) {
		return(value<=sup & value>=inf);
	}
	
	/**
	 * This method simply returns a string in math set notation: [minimum, maximum]
	 */
	public String toString() {
		return "["+ inf + ", " + sup +"]";	
	}
}
