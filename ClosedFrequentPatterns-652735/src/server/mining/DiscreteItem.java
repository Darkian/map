package mining;
import java.io.Serializable;

import data.DiscreteAttribute;

/**
 * This class, inherited by Item, manages a particular case: the item is discrete (not numbers).
 * @author manos
 *
 */
public class DiscreteItem extends Item implements Serializable{
	
	private transient static final long serialVersionUID = -2582956165570961067L;
	
	DiscreteItem() {
		super();
	}

	/**
	 * This constructor calls his super one to set values.
	 * @param a the new discrete attribute
	 * @param val the correspondant discrete value
	 */
	public DiscreteItem(DiscreteAttribute a, String val) {
		super(a,val);
	}
	
	/**
	 * An implementation of the abstract method defined in the top-level class.
	 */
	public boolean checkItemCondition(Object v) {
		if(v.equals(value))
			return true;
		else 
			return false;
	}
	
}
